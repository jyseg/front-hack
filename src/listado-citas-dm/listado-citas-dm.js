import { LitElement, html } from 'lit-element';

class ListadoCitasDm extends LitElement {
  static get properties() {
    return {
        citas: {type: Array},
        actualizaCitas: {type: Boolean}
    };
  }

  constructor() {
    super();

    this.citas = []; 
   
    this.actualizaCitas = false;

    //this.getCentrosData();

  }

  updated(changedProperties) {
    //console.log("updated");	
    if (changedProperties.has("citas")) {
        //console.log("Ha cambiado el valor de la propiedad citas en citas-dm");

        this.dispatchEvent(
            new CustomEvent("getCitasData", {
                    detail: {
                        citas: this.citas
                    }
                }
                
            )
        )
        
    }

    if (changedProperties.has("actualizaCitas")){

      if (this.actualizaCitas === true) {
        this.getCitasData();
      }

    }
  }

  getCitasData () {
    //console.log("getCentrosData");
    
/*
    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
        if (xhr.status === 200) {
            //console.log("petición correcta");
            
            let APIResponse = JSON.parse(xhr.responseText);
            this.centros = APIResponse;
            //console.log(xhr.responseText);

        } else {
            console.log("otro error" + xhr.status );
        }
    };

    xhr.open("GET", "https://swapi.dev/api/films/");
    xhr.send();
    //console.log("fin de getMovieData");*/
    
    

    this.citas = [
      {
        id: 12,
        centroid: 4,
        dia: "15/12/2020",
        hora: "14:50"
      }, {
        id: 1,
        centroid: 4,
        dia: "16/12/2020",
        hora: "14:50"
      }, {
        id: 2,
        centroid: 4,
        dia: "15/12/2020",
        hora: "15:50"
      }, {
        id: 3,
        centroid: 4,
        dia: "15/12/2020",
        hora: "14:30"
      }, {
        id: 4,
        centroid: 4,
        dia: "15/12/2020",
        hora: "17:50"
      }, {
        id: 5,
        centroid: 4,
        dia: "21/12/2020",
        hora: "11:50"
      }
    ];

}


  
  
}
customElements.define('listado-citas-dm', ListadoCitasD)
