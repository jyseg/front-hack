import { LitElement, html } from 'lit-element';

class CentrosDm extends LitElement {
  static get properties() {
    return {
        centros: {type: Array},
        actualizaCentros: {type: Boolean}
    };
  }

  constructor() {
    super();

    this.centros = []; 
   
    this.actualizaCentros = false;

    //this.getCentrosData();

  }

  updated(changedProperties) {
    //console.log("updated");	
    if (changedProperties.has("centros")) {
        //console.log("Ha cambiado el valor de la propiedad centros en centros-dm");

        this.dispatchEvent(
            new CustomEvent("getCentrosData", {
                    detail: {
                        centros: this.centros
                    }
                }
                
            )
        )
        
    }

    if (changedProperties.has("actualizaCentros")){

      if (this.actualizaCentros === true) {
        this.getCentrosData();
      }

    }
  }

  getCentrosData () {
    //console.log("getCentrosData");
    
/*
    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
        if (xhr.status === 200) {
            //console.log("petición correcta");
            
            let APIResponse = JSON.parse(xhr.responseText);
            this.centros = APIResponse;
            //console.log(xhr.responseText);

        } else {
            console.log("otro error" + xhr.status );
        }
    };

    xhr.open("GET", "https://swapi.dev/api/films/");
    xhr.send();
    //console.log("fin de getMovieData");*/
    
    

    this.centros = [
      {
        id: 12,
        nombre: "Quirón"
      }, {
        id: 10,
        nombre: "Quirón 1"
      }, {
        id: 2,
        nombre: "Fuensanta"
      }, {
        id: 3,
        nombre: "Maspalomas"
      }, {
        id: 4,
        nombre: "Playa larga"
      }, {
        id: 5,
        nombre: "Sanitas"
      }
    ];

}


  
  
}
customElements.define('centros-dm', CentrosDm)
