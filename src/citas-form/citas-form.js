import { LitElement, html } from 'lit-element';  
class CitasForm extends LitElement {
    static get properties() {
        return {			
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }
    
    constructor() {
        super();	

        this.resetFormData();
        
    
    }
    
    render() {
        return html`	
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
        crossorigin="anonymous" />
        <div>

          <form>
            <div class="row">
              <div class="col-md-4 mb-3">
                <label for="firstName">Nombre</label>
                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Nombre" 
                @input="${this.updateNombre}" required>

              </div>                  
              <div class="col-md-4 mb-3">
                <label for="lastName">Apellido/s</label>
                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Apellido/s" 
                @input="${this.updateApellido}" required>
              </div>
              <div class="col-md-4 mb-3">
                <label for="sexo">Sexo</label>
                <select class="custom-select d-block w-100" 
                placeholder="Seleccione..."
                @input="${this.updateSexo}" required>
                  <option>Seleccione...</option>  
                  <option>Femenino</option>
                  <option>Masculino</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 mb-3">
                <label>Edad</label>
                <input type="text" @input="${this.updateEdad}" class="form-control" 
                placeholder="Edad" id="personFormEdad"
                required>
              </div>
              <div class="col-md-4 mb-3">
                <label for="username">NIF</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">@</span>
                  </div>
                  <input type="text" class="form-control" id="docIdent" name="docIdent" placeholder="NIF" 
                  @input="${this.updateDNI}"
                  ?disabled="${this.editingPerson}"
                  required>
                </div>
              </div>
              <div id="errorDNI" class="d-none col-md-4 mb-3">
                <br />&nbsp;&nbsp;Por favor, intorduzca un DNI válido
              </div>
              
            </div>
            <div class="row">
              <div class="form-check col-md-4 mb-3">
                <input class="form-check-input" type="radio" name="filtrado" id="filtCentro" value="centro" 
                checked>
                <label class="form-check-label" for="filtCentro">
                  Obtener citas disponibles para un centro concreto
                </label>
              </div>
              <div class="form-check col-md-4 mb-3">
                <input type="text" class="form-control" id="nomcentro" name="nomcentro" placeholder="Centro" 
                .value="${this.person.centro}"
                @input="${this.updateCentro}">
              </div>
              <div class="form-check col-md-4 mb-3">
                <button @click="${this.findCentros}" class="btn btn-primary"><strong>Buscar Centro</strong></button>
              </div>
              
              <div class="form-check col-md-4 mb-3">
                <input class="form-check-input" type="radio" name="filtrado" id="filtFecha" value="fecha">
                <label class="form-check-label" for="filtFecha">
                  Obtener citas disponibles a partir de una fecha concreta
                </label>
              </div>
              <div class="form-check col-md-4 mb-3">
                <input type="date" class="form-control disabled" id="nomfecha" name="nomfecha" placeholder="" 
                .value="${this.person.fecha}"
                @input="${this.updateFecha}">
              </div>
              <div class="form-check col-md-4 mb-3"></div>
            </div>

                                          
            <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
            <button @click="${this.storePerson}" class="btn btn-success" type="submit"><strong>Buscar cita</strong></button>
                   
          </form>
        </div>			
        `;
    }
    
    
    goBack(e) {
        //console.log("goBack");	  
        e.preventDefault();	
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
        this.resetFormData();
    }
    
    updateNombre(e) {
        
        this.person.nombre = e.target.value;
    }

    updateApellido(e) {
        
        this.person.apellido = e.target.value;
    }
    
    updateDNI(e) {
          this.person.dni = e.target.value;
          this.shadowRoot.getElementById("errorDNI").classList.add("d-none");
    }

    updateSexo(e) {
        this.person.sexo = e.target.value;
  }
    
    updateEdad(e) {
        this.person.edad = e.target.value;
    }

    updateCentro(e) {
        this.person.centro = e.target.value;
        this.person.fecha = "";
    }

    updateFecha(e) {
        this.person.fecha = e.target.value;
        this.person.centro = "";
    }
    
    storePerson(e) {
        //console.log("storePerson");
        e.preventDefault();
            
        console.log("La propiedad nombre vale " + this.person.nombre);
        console.log("La propiedad apellido vale " + this.person.apellido);
        console.log("La propiedad dni vale " + this.person.dni);
        console.log("La propiedad sexo vale " + this.person.sexo);
        console.log("La propiedad edad vale " + this.person.edad);
        console.log("La propiedad centro vale " + this.person.centro);
        console.log("La propiedad fecha vale " + this.person.fecha);
        //console.log(this.person); 
        
        //let dni_valido = this.validar_dni_nif_nie(this.person.dni);
        let dni_valido = true;


        if (dni_valido === true) {
            this.dispatchEvent(new CustomEvent("persona-form-store",{
                detail: {
                    person:  {
                            nombre: this.person.nombre,
                            apellido: this.person.apellido,
                            sexo: this.person.sexo,
                            edad: this.person.edad,
                            dni: this.person.dni,
                            centro: this.person.centro,
                            idcentro: this.person.idcentro,
                            fecha: this.person.fecha
                            
                        },
                    editingPerson: this.editingPerson
                    }
                })
            );

            this.resetFormData();
            
        } else {
            this.shadowRoot.getElementById("errorDNI").classList.remove("d-none");


        }

        
    }

    resetFormData (){

        this.person = {};
        this.person.nombre = "";		
        this.person.apellido = "";	
        this.person.dni = "";
        this.person.sexo = "";	
        this.person.edad = "";
        this.person.centro = "";
        this.person.idcentro = "";
        this.person.fecha = "";
        this.editingPerson = false;
        this.showErrorData = false;
    }

    validar_dni_nif_nie(value){
 
        var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
        var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
        var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
        var str = value.toString().toUpperCase();

        if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

        var nie = str
            .replace(/^[X]/, '0')
            .replace(/^[Y]/, '1')
            .replace(/^[Z]/, '2');

        var letter = str.substr(-1);
        var charIndex = parseInt(nie.substr(0, 8)) % 23;

        if (validChars.charAt(charIndex) === letter) return true;

        return false;
    }

    findCentros(e) {
      
      e.preventDefault();	
      this.dispatchEvent(new CustomEvent("buscar-centros",{
        detail: {
          person:  {
            nombre: this.person.nombre,
            apellido: this.person.apellido,
            sexo: this.person.sexo,
            edad: this.person.edad,
            dni: this.person.dni,
            centro: this.person.centro,
            idcentro: this.person.idcentro,
            fecha: this.person.fecha            
        },
        }
      }));
      
  }

    
    
}
customElements.define('citas-form', CitasForm)