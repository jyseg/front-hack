import { LitElement, html } from 'lit-element';  
	class CentrosFichaListado extends LitElement { 


	static get properties() {
		return {
			nombre: {type: String},
			id: {type: Number}
		};
	}

	constructor() {
		super();			
	}


	render() {
		return html`
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
		integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
		crossorigin="anonymous" />
			<div class="card h-100">
				<div class="card-body bg-primary">
					<div class="card-text">
						Cód.: ${this.id}</p>
					</div>
					<div class="card-title">
						${this.nombre}
					</div>
				</div>
				<div class="card-footer">
					<button @click="${this.selectCentro}" class="btn btn-info"><strong>Seleccionar</strong></button>
				</div>				
			</div>
	`;
}

	

	selectCentro(e) {	
		//console.log("moreInfo en persona-ficha-listado");
		//console.log("Se ha seleccionado el centro de nombre " + this.nombre + " " + this.id); 

		this.dispatchEvent(
			new CustomEvent("seleccion-centro", {
					detail: {
						id: this.id,
						nombre: this.nombre
					}
				}
			)
		);
	}
}  
customElements.define('centros-ficha-listado', CentrosFichaListado)