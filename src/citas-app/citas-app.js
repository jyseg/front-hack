import { LitElement, html} from 'lit-element';
import '../citas-header/citas-header.js';
import '../citas-footer/citas-footer.js';
import '../citas-main/citas-main.js';


class CitasApp extends LitElement{


    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
	      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
	      crossorigin="anonymous" />
        
        <citas-header></citas-header>
        <div class="col-md-2 mb-3"></div>
        <div class="col-md-10 mb-3">
            <citas-main></citas-main>
        </div>
        <citas-footer></citas-footer>

        `;
      }

      
}

customElements.define('citas-app', CitasApp)