import { LitElement, html } from 'lit-element'; 
import '../centros-ficha-listado/centros-ficha-listado.js'; 
import '../centros-dm/centros-dm.js';

class CentrosMain extends LitElement {
	static get properties() {
		return {			
      centros: {type: Array},
      obtieneCentros: {type: Boolean}
		};
	}			

	constructor() {
		super();
	
    this.centros = [];
    this.obtieneCentros = false;
		
  }
  
  render() {
     return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
      crossorigin="anonymous">				
      <h2 class="text-center">Centros</h2>
        
        <div class="row row-cols-1 row-cols-sm-6" id="centrosList">
          ${this.centros.map(
            centro => 
            html`<centros-ficha-listado 
                nombre="${centro.nombre}" 
                id="${centro.id}" 
                @seleccion-centro="${this.selectCentro}"> 
                </centros-ficha-listado>`
           )}
         </div>
        
        <centros-dm id="listaCentros" @getCentrosData="${this.getCentrosData}" ></centros-dm>
      `;
    }

    updated(changedProperties) {
      //console.log("updated");	
      if (changedProperties.has("obtieneCentros")) {
        //console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
        if (this.obtieneCentros === true) {
          this.actualizaCentros();
        } 
      }
    }


    getCentrosData(e){

      //console.log("oteniendo datos")
      //console.log(e.detail.centros)
  
      this.centros = e.detail.centros
      //this.requestUpdate();
    }

    selectCentro(e) {	
      
      //console.log("centros main Se ha seleccionado el centro de nombre " + e.detail.nombre + " " + e.detail.id); 
  
      this.dispatchEvent(
        new CustomEvent("centro-seleccionado", {
            detail: {
              id: e.detail.id,
              nombre: e.detail.nombre
            }
          }
        )
      );
    }

    actualizaCentros() {

      this.shadowRoot.getElementById("listaCentros").actualizaCentros = true;
    }


}
customElements.define('centros-main', CentrosMain)
