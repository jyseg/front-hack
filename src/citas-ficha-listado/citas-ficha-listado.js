import { LitElement, html } from 'lit-element';  
	class CitasFichaListado extends LitElement { 


	static get properties() {
		return {
			id: {type: Number},
			centroid: {type: String},
			dia: {type: Date},
			hora: {type: String}
		};
	}

	constructor() {
		super();			
	}


	render() {
		return html`
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
		integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
		crossorigin="anonymous" />
			<div class="card h-100">
				<div class="card-body">
					<div class="card-text  bg-primary">
						Cód.: ${this.id}</p>
					</div>
					<div class="card-title  bg-primary">
						${this.centroid}
					</div>
					<div class="card-title">
						${this.dia}
					</div>
					<div class="card-title">
						${this.hora}
					</div>
				</div>
				<div class="card-footer">
					<button @click="${this.selectCitas}" class="btn btn-info"><strong>Seleccionar</strong></button>
				</div>				
			</div>
	`;
}

	

	selectCitas(e) {	
		//console.log("moreInfo en persona-ficha-listado");
		//console.log("Se ha seleccionado el centro de nombre " + this.nombre + " " + this.id); 

		this.dispatchEvent(
			new CustomEvent("seleccion-cita", {
					detail: {
						id: this.id,
						centroid: this.centroid,
						dia: this.dia,
						hora: this.hora
					}
				}
			)
		);
	}
}  
customElements.define('citas-ficha-listado', CitassFichaListado)