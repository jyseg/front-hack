import { LitElement, html} from 'lit-element';

import '../citas-form/citas-form.js';
import '../centros-main/centros-main.js';

class CitasMain extends LitElement{
  
  static get properties() {
		return {			
      person: {type: Object},
      showListadoCentros: {type: Boolean},
      showListadoCitas: {type: Boolean},
      showConfirmCita: {type: Boolean}
		};
	}			

	constructor() {
		super();
	
    this.centros = [];
    this.showListadoCentros = false;
    this.showListadoCitas = false;
    this.showConfirmCita = false;
		
  }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
	      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
	      crossorigin="anonymous" />
        <citas-form id="formCitas"
        @buscar-centros="${this.findCentro}"
        @persona-form-store="${this.findCita}"></citas-form>
        <centros-main id="centrosList" class="d-none"
        @centro-seleccionado="${this.actualizaCentro}"></centros-main>
        <listado-citas id="citasList" class="d-none"
        @cita-seleccionada="${this.pideCita}"></listado-citas>
        
			  
        `;
      }

      updated(changedProperties) {
        //console.log("updated");	
        if (changedProperties.has("showListadoCentros")) {
          //console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
          if (this.showListadoCentros === true) {
            this.showCentrosList();
          } else {
            this.showformCitas();
          }
         
        } 

        if (changedProperties.has("showListadoCitas")) {
          if (this.showListadoCitas === true) {
            this.showCitasList();
          } else {
            this.showformCitas();
          }

        }
      }


      showCentrosList() {
        //console.log("showPersonList");
        //console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("centrosList").classList.remove("d-none");
        this.shadowRoot.getElementById("citasList").classList.add("d-none");
        this.shadowRoot.getElementById("formCitas").classList.add("d-none");	  
        }

        showCitasList() {
          //console.log("showPersonList");
          //console.log("Mostrando listado de personas");
          this.shadowRoot.getElementById("centrosList").classList.add("d-none");
          this.shadowRoot.getElementById("citasList").classList.remove("d-none");
          this.shadowRoot.getElementById("formCitas").classList.add("d-none");	  
          }
      
        showformCitas() {
        //console.log("showPersonFormData");
        //console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("centrosList").classList.add("d-none");
        this.shadowRoot.getElementById("citasList").classList.add("d-none");
        this.shadowRoot.getElementById("formCitas").classList.remove("d-none");	 	  
        }

        findCentro(e){

          this.showListadoCentros = true;
          this.person = e.detail.person;
          //console.log(this.person)
          this.shadowRoot.getElementById("centrosList").obtieneCentros = true;

        }
        
        findCita(e){

          this.showListadoCitas = true;
          this.person = e.detail.person;
          //console.log(this.person)
          this.shadowRoot.getElementById("citasList").obtieneCitas = true;

        }

        actualizaCentro(e) {

                
          this.person.idcentro = e.detail.id;
          this.person.centro = e.detail.nombre;

          //console.log(this.person.centro)

          this.shadowRoot.getElementById("formCitas").person = this.person;

          //this.requestUpdate();
          this.showListadoCentros = false;



        }

        pideCita(e) {

          console.log(e.detail)
        }

      
}

customElements.define('citas-main', CitasMain)