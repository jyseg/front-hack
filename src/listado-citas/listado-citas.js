import { LitElement, html } from 'lit-element'; 
import '../citas-ficha-listado/citas-ficha-listado.js'; 
import '../listado-citas-dm/listado-citas-dm.js';

class ListadoCitas extends LitElement {
	static get properties() {
		return {			
      ListadoCitas: {type: Array},
      obtieneCitas: {type: Boolean},
      person: {type: Object}
		};
	}			

	constructor() {
    super();
    this.person = {};
	  this.citas = [];
    this.obtieneCitas = false;
		
  }
  
  render() {
     return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" 
      crossorigin="anonymous">				
      <h2 class="text-center">Citas</h2>
        
        <div class="row row-cols-1 row-cols-sm-6" id="citasList">
          ${this.citas.map(
            cita => 
            html`<citas-ficha-listado 
                id="${citas.id}" 
                centroid="${citas.centroid}" 
                dia="${citas.dia}" 
                hora="${citas.hora}" 
                @seleccion-cita="${this.selectCitas}"> 
                </citas-ficha-listado>`
           )}
         </div>
        
        <listado-citas-dm id="listaCitas" @getCitasData="${this.getCitasData}" ></listado-citas-dm>
      `;
    }

    updated(changedProperties) {
      //console.log("updated");	
      if (changedProperties.has("obtieneCitas")) {
        //console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
        if (this.obtieneCitas === true) {
          this.actualizaCitas();
        } 
      }
    }


    getCitasData(e){

      //console.log("oteniendo datos")
      //console.log(e.detail.centros)
  
      this.citas = e.detail.citas
      //this.requestUpdate();
    }

    selectCita(e) {	
      
      //console.log("centros main Se ha seleccionado el centro de nombre " + e.detail.nombre + " " + e.detail.id); 
  
      this.dispatchEvent(
        new CustomEvent("cita-seleccionada", {
            detail: {
              id: e.detail.id,
              nombre: e.detail.nombre
            }
          }
        )
      );
    }

    actualizaCitas() {

      this.shadowRoot.getElementById("listaCitas").actualizaCitas = true;
      this.shadowRoot.getElementById("listaCitas").person = this.person;
    }


}
customElements.define('centros-main', CentrosMain)
